const FETCH_ALL_ITEMS = 'FETCH_ALL_ITEMS';
const NEW_ITEMS = 'NEW_ITEMS';
const UPDATE_ITEM = 'UPDATE_ITEM';
const REMOVE_ITEMS = 'REMOVE_ITEMS';

const ITEM_STOCK_UPDATED = 'ITEM_STOCK_UPDATED';
const ALL_ITEMS_FETCHED = 'ALL_ITEMS_FETCHED';

export default function reducer(state = [], action) {
  switch (action.type) {
    case ALL_ITEMS_FETCHED:
      return Object.keys(action.items)
      .map(key => action.items[key]);
    case NEW_ITEMS:
      return [
        ...state,
        action.newItem
      ];
    case UPDATE_ITEM:
      return state
      .map(item => (item.id === action.id ?
        { ...item, ...action.itemProps } : item));
    case REMOVE_ITEMS:
      return state
      .filter(item => action.items.indexOf(item) === -1);
    case ITEM_STOCK_UPDATED:
      return state.map(item => (item.id === action.item.id ? action.item : item));
    default:
      return state;
  }
}

export function addNewItem(newItem) {
  return (dispatch, _, { ref, timestamp }) => {
    ref.child(`inventory/${newItem.id}`)
    .set({ ...newItem, timestamp })
    .then(() => dispatch({ type: NEW_ITEMS, newItem }));
  };
}

export function updateItem(id, itemProps) {
  return (dispatch, _, { ref }) => {
    if (itemProps.id) {
      const oldChild = ref.child(`inventory/${id}`);

      oldChild.once('value', snap => {
        const newChildVal = Object.assign(snap.val(), itemProps);

        ref.child(`inventory/${itemProps.id}`).set(newChildVal);
        oldChild.remove()
        .then(() => dispatch({ type: UPDATE_ITEM, id, itemProps }));
      });
    } else {
      ref.child(`inventory/${id}`)
      .update(itemProps)
      .then(() => dispatch({ type: UPDATE_ITEM, id, itemProps }));
    }
  };
}

export function removeItems(items) {
  return (dispatch, _, { ref }) => {
    const itemsToDel = {};
    items.forEach(item => (itemsToDel[item.id] = null));

    ref.child('inventory')
    .update(itemsToDel)
    .then(() => dispatch({ type: REMOVE_ITEMS, items }));
  };
}

export function fetchInventoryItems() {
  return (dispatch, getState, { ref }) => {
    dispatch({ type: FETCH_ALL_ITEMS });

    ref.child('inventory').once('value')
    .then(snap => dispatch({ type: ALL_ITEMS_FETCHED, items: snap.val() || [] }))
    .then(() => {
      ref.child('inventory').on('child_changed', snap => {
        const item = snap.val();
        const origItem = getState().inventory
        .items.reduce((prev, curr) => (prev.id === item.id ? prev : curr));

        if (origItem.stock !== item.stock) {
          dispatch({ type: ITEM_STOCK_UPDATED, item });
        }
      });
    });
  };
}
