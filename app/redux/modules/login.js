import { hashHistory } from 'react-router';

const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
const LOGIN_ERROR = 'LOGIN_ERROR';
const REMOVE_ERORR = 'REMOVE_ERORR';

const init = {
  email: null,
  error: {
    msg: null,
    code: null
  }
};

export default function reducer(state = init, action) {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return {
        email: 'email',
        error: {
          msg: null,
          code: null,
        }
      };
    case LOGIN_ERROR:
      return {
        email: null,
        error: {
          msg: action.err.message,
          code: action.err.code,
        }
      };
    case REMOVE_ERORR:
      return {
        email: null,
        error: {
          msg: null,
          code: null,
        }
      };
    default:
      return state;
  }
}

export function removeError() {
  return {
    type: REMOVE_ERORR
  };
}

export function verifyCreds(email, password) {
  return (dispatch, _, { auth }) => {
    auth().signInWithEmailAndPassword(email, password)
    .then(authData => {
      hashHistory.push('/dashboard');
      dispatch({ type: LOGIN_SUCCESS, authData });
    })
    .catch(err => dispatch({ type: LOGIN_ERROR, err }));
  };
}
