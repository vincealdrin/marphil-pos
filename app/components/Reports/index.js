import React, { Component, PropTypes } from 'react';
import Radium from 'radium';

import Paper from 'material-ui/Paper';
import styles from './styles';

@Radium
export default class Reports extends Component {
  static propTypes = {
    reports: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  }

  componentWillMount() {
    this.props.actions.fetchAllReports();
  }

  render() {
    const { reports } = this.props;

    return (
      <div>
      {Object.keys(reports).map((dayMonth) => (
        <div>
          ({dayMonth}) Day Total: $
          {Object.keys(reports[dayMonth])
          .map((timestamp) => reports[dayMonth][timestamp]
          .map((item) => item.price * item.quantity)
          .reduce((p, c) => p + c))
          .reduce((p, c) => p + c)}

          {Object.keys(reports[dayMonth]).reverse().map((timestamp) => (
            <ul>
              <li>
                ({new Date(timestamp.slice(0, -3) * 1000).toLocaleTimeString()})
                Cart Total: ${reports[dayMonth][timestamp]
                  .map((item) => item.quantity * item.price)
                  .reduce((p, c) => p + c)}
                <ul>
                  <li>
                    {reports[dayMonth][timestamp].map((item) => (
                      <li>
                        {item.name} x{item.quantity}
                      </li>
                    ))}
                  </li>
                </ul>
              </li>
            </ul>
          ))}
        </div>
      ))}
      </div>
    );
  }
}
