import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import Radium from 'radium';

import AppBar from 'material-ui/AppBar';
import Paper from 'material-ui/Paper';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';

const styles = {
  bodyStyle: {
    width: '100vw',
    height: '100vh',
    backgroundColor: '#EDECEC',
    overflow: 'hidden',
  },
  sideNavContainer: {
    position: 'relative',
    height: '90.5vh',
    padding: 0,
  },
  sideNav: {
    width: '100%',
  },
  mainContent: {
    height: '90vh',
    overflowY: 'auto',
  },
  appbar: {
    height: '10vh',
  },
  hide: {
    display: 'none'
  }
};

@Radium
export default class DashboardLayout extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired
  }

  constructor() {
    super();

    this.state = {
      isExpanded: false
    };
  }

  _toggleDrawer = () => {
    this.setState({ isExpanded: !this.state.isExpanded });
  }

  render() {
    return (
      <div className="row" style={styles.bodyStyle}>
        <AppBar
          title="Admin Dashboard"
          onLeftIconButtonTouchTap={this._toggleDrawer}
          style={styles.appbar}
        />
        <Paper
          className={this.state.isExpanded ? 'col-xs-1' : 'hidden'}
          style={styles.sideNavContainer}
        >
          <Menu>
            <Link to="/inventory">
              <MenuItem>
                inventory
              </MenuItem>
            </Link>
            <Link to="/accounts">
              <MenuItem>
                accounts
              </MenuItem>
            </Link>
            <Link to="/analytics">
              <MenuItem>
                analytics
              </MenuItem>
            </Link>
            <Link to="/reports">
              <MenuItem>
                reports
              </MenuItem>
            </Link>
          </Menu>
        </Paper>
        <div className={this.state.isExpanded ? 'col-md-1' : null}></div>
        <div className={this.state.isExpanded ? 'col-md-11' : 'col-md-12'} style={styles.mainContent}>
          {this.props.children}
        </div>
      </div>
    );
  }
}
