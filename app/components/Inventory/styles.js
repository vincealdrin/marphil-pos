export default {
  superheader: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  actions: {
    display: 'inline-flex'
  },
  paper: {
    width: '85vw',
    height: '85vh',
  },
  container: {
    display: 'flex',
    width: '100%',
    height: '90vh',
    justifyContent: 'center',
    alignItems: 'center',
  },
  addButton: {
    position: 'fixed',
    right: 30,
    bottom: 15,
  },
  dialog: {
    width: 450,
    height: '100%'
  },
  textfield: {
    width: '100%',
  },
  catselect: {
    width: '100%',
  },
  catbutton: {
    postition: 'relative',
    right: 0
  },
  title: {
    display: 'inline-flex',
    width: '100%',
    height: '90px',
    justifyContent: 'space-between',
    alignItems: 'center',
  }
};
