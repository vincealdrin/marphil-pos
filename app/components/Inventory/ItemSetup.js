import React, { Component, PropTypes } from 'react';
import { reduxForm } from 'redux-form';

import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import AutoComplete from 'material-ui/AutoComplete';
import styles from './styles';

@reduxForm({
  form: 'new_inventory_item',
  fields: [
    'id',
    'name',
    'category',
    'newCategory',
    'price',
    'stock',
    'brand',
    'supplier',
    'description'
  ]
},
(state) => ({
  initialValues: {
    id: Math.max(
      ...state.inventory.items.map(item => item.id)
    ) + 1,
    price: 1,
    stock: 1
  }
}))
export default class ItemSetup extends Component {
  static propTypes = {
    fields: PropTypes.object.isRequired,
    values: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
    categories: PropTypes.array.isRequired,
    brands: PropTypes.array.isRequired,
    suppliers: PropTypes.array.isRequired,
    disabled: PropTypes.bool.isRequired,
    addNewItem: PropTypes.func,
    addNewCategory: PropTypes.func,
    removeCategory: PropTypes.func,
    fieldError: PropTypes.string,
  }

  constructor() {
    super();

    this.state = { openDialog: false };
  }

  _onFocus = (e) => {
    const { fields } = this.props;

    e.preventDefault();
    e.stopPropagation();
    fields.newCategory.onFocus();
  }

  _onBlurValidate = (e) => {
    const { fields, actions } = this.props;

    actions.validateField(e.target.name, e.target.value);
    fields.id.onBlur();
  }

  _addNewItem = (e) => {
    const {
      values,
      actions,
    } = this.props;

    e.preventDefault();
    actions.addNewItem({ ...values, newCategory: null });
  }

  _removeCategory = category => {
    const { removeCategory } = this.props;

    removeCategory(category);
  }

  _handleCategoriesDialog = (e) => {
    e.stopPropagation();
    e.preventDefault();
  }

  _handleDialogState = (e) => {
    e.preventDefault();
    this.setState({ openDialog: !this.state.openDialog });
  }

  render() {
    const {
      fields,
      categories,
      brands,
      suppliers,
      disabled,
      fieldError,
    } = this.props;

    const actions = [
      <FlatButton
        label="Cancel"
        onTouchTap={this._handleDialogState}
        primary
      />,
      <FlatButton
        type="submit"
        label="Submit"
        onTouchTap={this._addNewItem}
        primary
      />
    ];

    return (
      <div>
        <FlatButton
          label="Add"
          onTouchTap={this._handleDialogState}
          disabled={disabled}
          primary
        />
        <Dialog
          title="Item Setup"
          actions={actions}
          open={this.state.openDialog}
          contentStyle={styles.dialog}
          titleStyle={styles.title}
          autoScrollBodyContent
        >
          <form onSubmit={this._addNewItem}>
            <TextField
              {...fields.id}
              type="number"
              min={1}
              step="any"
              name="id"
              floatingLabelText="Item ID"
              errorText={fieldError}
              onBlur={this._onBlurValidate}
              fullWidth
              autoFocus
            />
            <br />
            <TextField
              {...fields.name}
              floatingLabelText="Item Name"
              fullWidth
            />
            <br />
            <AutoComplete
              {...fields.category}
              floatingLabelText="Category"
              filter={AutoComplete.fuzzyFilter}
              dataSource={categories}
              onUpdateInput={val => fields.category.onChange(val)}
              onNewRequest={val => {
                setTimeout(() => this.refs.brandField.focus(), 500);
                fields.category.onChange(val);
              }}
              fullWidth
            />
            <br />
            <AutoComplete
              {...fields.brand}
              floatingLabelText="Brand"
              ref="brandField"
              filter={AutoComplete.fuzzyFilter}
              dataSource={brands}
              onUpdateInput={val => fields.brand.onChange(val)}
              onNewRequest={val => {
                setTimeout(() => this.refs.supplierField.focus(), 500);
                fields.brand.onChange(val);
              }}
              fullWidth
            />
            <br />
            <AutoComplete
              {...fields.supplier}
              floatingLabelText="Supplier"
              ref="supplierField"
              filter={AutoComplete.fuzzyFilter}
              dataSource={suppliers}
              onUpdateInput={val => fields.supplier.onChange(val)}
              onNewRequest={val => {
                setTimeout(() => this.refs.priceField.focus(), 500);
                fields.supplier.onChange(val);
              }}
              fullWidth
            />
            <br />
            <TextField
              {...fields.price}
              type="number"
              min={1}
              step="any"
              ref="priceField"
              floatingLabelText="Price"
              fullWidth
            />
            <br />
            <TextField
              {...fields.stock}
              type="number"
              min={1}
              step="any"
              floatingLabelText="Stock"
              fullWidth
            />
            <br />
            <TextField
              {...fields.description}
              type="textarea"
              floatingLabelText="Description"
              fullWidth
            />
            <input className="hidden" type="submit" />
          </form>
        </Dialog>
      </div>
    );
  }
}
