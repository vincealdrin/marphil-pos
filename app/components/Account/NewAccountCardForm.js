import React, { Component, PropTypes } from 'react';
import { reduxForm } from 'redux-form';
import Radium from 'radium';

import AccountCardForm from './AccountCardForm';
import IconButton from 'material-ui/IconButton';
import AddCircleOutline from 'material-ui/svg-icons/content/add-circle-outline';

const styles = {
  addNew: {
    display: 'flex',
    width: 310,
    height: 300,
    justifyContent: 'center',
    alignItems: 'center',
    border: '3px dashed #29B6F6',
    borderRadius: 3,
    marginTop: 20,
    ':hover': {
      backgroundColor: '#E1F5FE',
      cursor: 'pointer',
    }
  },
  largeIcon: {
    width: 60,
    height: 60,
  },
  large: {
    width: '100%',
    height: '100%',
    padding: 30,
  },
};

@Radium
export default class AccountCard extends Component {
  static propTypes = {

  }

  constructor() {
    super();

    this.state = { showForm: false };
  }


  _toggleCardForm = () => {
    this.setState({ showForm: !this.state.showForm });
  }

  render() {
    const { editAccount } = this.props;

    return (
      this.state.showForm ?
      <AccountCardForm editAccount={editAccount} /> :
      <div style={styles.addNew}>
        <IconButton
          onTouchTap={this._toggleCardForm}
          iconStyle={styles.largeIcon}
          style={styles.large}
        >
          <AddCircleOutline />
        </IconButton>
      </div>
    );
  }
}
