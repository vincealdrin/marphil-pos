import React, { Component, PropTypes } from 'react';
import Radium from 'radium';

import AccountCard from './AccountCard';
import AccountCardForm from './AccountCardForm';
import NewAccountCardForm from './NewAccountCardForm';

const styles = {
  accountContainer: {
    display: 'flex',
    height: '90vh',
    justifyContent: 'space-around',
    flexWrap: 'wrap',
  },
};

@Radium
export default class Accounts extends Component {
  static propTypes = {

  }

  componentWillMount() {
    this.props.actions.fetchAllAccounts();
  }


  render() {
    const { 
      actions,
      users,
      accountCardEdit,
    } = this.props;

    return (
      <div style={styles.accountContainer}>
        {users.map(account => {
          if (account.email === accountCardEdit) {
            return <AccountCardForm key={account.email} account={account} />;
          }

          return (
            <AccountCard
              key={account.email}
              account={account}
              editAccount={actions.editAccount}
              removeAccount={actions.removeAccount}
            />
          );
        })}
        <NewAccountCardForm newAccount={actions.newAccount} editAccount={actions.editAccount} />
      </div>
    );
  }
}
