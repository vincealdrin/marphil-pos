import React, { Component, PropTypes } from 'react';
import Radium from 'radium';

import Paper from 'material-ui/Paper';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import CommunicationCall from 'material-ui/svg-icons/communication/call';
import CommunicationEmail from 'material-ui/svg-icons/communication/email';
import Lock from 'material-ui/svg-icons/action/lock';
import IconButton from 'material-ui/IconButton';
import { List, ListItem } from 'material-ui/List';
import Divider from 'material-ui/Divider';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import Avatar from 'material-ui/Avatar';

const styles = {
  paper: {
    width: 310,
    height: 300,
    marginTop: 20,
  }
};

@Radium
export default class AccountCard extends Component {
  static propTypes = {
    account: PropTypes.object.isRequired,
    removeAccount: PropTypes.func.isRequired,
    editAccount: PropTypes.func.isRequired,
  }

  _removeAccount = () => {
    const { account, removeAccount } = this.props;


    removeAccount(account);
  }

  render() {
    const { account, editAccount } = this.props;

    const IconButtonElement = (
      <IconButton
        tooltip="more"
        tooltipPosition="bottom-left"
        touch
      >
        <MoreVertIcon />
      </IconButton>
    );

    return (
      <Paper style={styles.paper}>
        <List>
          <ListItem
            leftAvatar={
              <Avatar
                src="https://scontent.fmnl4-5.fna.fbcdn.net/v/t1.0-9/12316584_1100176376668658_969451991069478777_n.jpg?oh=50b6cbf8b68c9693ee00346a5307c346&oe=57DC512D"
                size={45}
              />
            }
            rightIconButton={
              <IconMenu iconButtonElement={IconButtonElement}>
                <MenuItem onTouchTap={() => editAccount(account.email)}>Edit</MenuItem>
                <MenuItem onTouchTap={this._removeAccount}>Remove</MenuItem>
              </IconMenu>
            }
            primaryText={name}
            secondaryText="Name"
            disable
          />
          <Divider inset />
          <ListItem
            leftIcon={<CommunicationCall />}
            primaryText={account.mobile}
            secondaryText="Mobile"
          />
          <ListItem
            leftIcon={<CommunicationEmail />}
            primaryText={account.email}
            secondaryText="Email"
          />
          <ListItem
            leftIcon={<Lock />}
            primaryText={account.password}
            secondaryText="Password"
          />
        </List>
      </Paper>
    );
  }
}
