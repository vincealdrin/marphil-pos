import React, { Component, PropTypes } from 'react';
import { reduxForm } from 'redux-form';
import Radium from 'radium';

import Paper from 'material-ui/Paper';
import CommunicationCall from 'material-ui/svg-icons/communication/call';
import CommunicationEmail from 'material-ui/svg-icons/communication/email';
import Lock from 'material-ui/svg-icons/action/lock';
import IconButton from 'material-ui/IconButton';
import Done from 'material-ui/svg-icons/action/done';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

const styles = {
  paper: {
    width: 310,
    height: 300,
    marginTop: 20,
    border: '3px dashed #29B6F6',
  },
  textfield: {
    width: '100%',
  },
  addNew: {
    display: 'flex',
    width: 310,
    height: 300,
    justifyContent: 'center',
    alignItems: 'center',
    border: '3px dashed #29B6F6',
    borderRadius: 3,
    marginTop: 20,
    ':hover': {
      backgroundColor: '#E1F5FE',
      cursor: 'pointer',
    }
  },
  largeIcon: {
    width: 60,
    height: 60,
  },
  large: {
    width: '100%',
    height: '100%',
    padding: 30,
  },
};

@Radium
@reduxForm({
  form: 'new_account',
  fields: [
    'name',
    'photo',
    'mobile',
    'email',
    'password',
  ]
})
export default class AccountCard extends Component {
  static propTypes = {
    actions: PropTypes.array.isRequired,

  }

  _addNewAccount = () => {
    const { newAccount, values, editAccount, } = this.props;

    newAccount(values); // TODO: ACCOUNT PHOTO
    this.setState({ showForm: !this.state.showForm });
    editAccount('');
  }

  render() {
    const {
      fields,
      account,
    } = this.props;

    return (
      <Paper style={styles.paper}>
        <form onSubmit={this._addNewAccount}>
          <TextField
            {...fields.name}
            floatingLabelText="Name"
            value={account ? account.name : null}
            style={styles.textfield}
          />
          <TextField
            {...fields.image}
            type="file"
            floatingLabelText="Image"
            value={account ? account.image : null}
            style={styles.textfield}
          />
          <TextField
            {...fields.mobile}
            floatingLabelText="Mobile"
            value={account ? account.mobile : null}
            style={styles.textfield}
          />
          <TextField
            {...fields.email}
            floatingLabelText="Email"
            value={account ? account.email : null}
            style={styles.textfield}
          />
          <TextField
            {...fields.password}
            floatingLabelText="Password"
            value={account ? account.password : null}
            style={styles.textfield}
          />
          <RaisedButton type="submit" label="Done" primary />
        </form>
      </Paper>
    );
  }
}
