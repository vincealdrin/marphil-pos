import React, { Component, PropTypes } from 'react';
import { reduxForm } from 'redux-form';

import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

const styles = {
  form: {
    padding: '20px'
  },
  textfield: {
    width: '100%'
  },
  submit: {
    width: '100%'
  }
};

@reduxForm({
  form: 'login',
  fields: [
    'email',
    'password',
  ]
})
export default class LoginForm extends Component {
  static propTypes = {
    label: PropTypes.string.isRequired,
    error: PropTypes.object.isRequired,
    verifyCreds: PropTypes.func.isRequired,
  }

  userAuth = (e) => {
    const { email, password } = this.props.values;

    e.preventDefault();
    this.props.verifyCreds(email, password);
  }

  render() {
    const {
      fields,
      resetForm,
      label,
    } = this.props;

    return (
      <form
        name={label}
        style={styles.form}
        onSubmit={this.userAuth}
      >
        <TextField
          type="email"
          ref={`${label}Email`}
          floatingLabelText="Email"
          style={styles.textfield}
          {...fields.email}
          autoFocus
          required
        />
        <br />
        <TextField
          type="password"
          ref={`${label}Password`}
          floatingLabelText="Password"
          style={styles.textfield}
          {...fields.password}
          required
        />
        <br />
        <RaisedButton
          type="submit"
          label="Login"
          style={styles.submit}
          secondary
        />
      </form>
    );
  }
}
