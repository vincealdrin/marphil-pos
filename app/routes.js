import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './App';
import LoginContainer from './containers/LoginContainer';
import DashboardLayout from './components/DashboardLayout';
import Inventory from './containers/InventoryContainer';
import Accounts from './containers/AccountsContainer';
import Analytics from './containers/AnalyticsContainer';
import Reports from './containers/ReportsContainer';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={DashboardLayout} />
    <Route path="dashboard" component={DashboardLayout}>
      <IndexRoute component={Inventory} />
      <Route path="/inventory" component={Inventory} />
      <Route path="/accounts" component={Accounts} />
      <Route path="/analytics" component={Analytics} />
      <Route path="/reports" component={Reports} />
    </Route>
  </Route>
);

